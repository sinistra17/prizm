/*
Name: Pixel Map Genertor | amCharts
Description: API to handle the UI changes
Author: Antanas Marcelionis, Benjamin Maertz, amCharts
Version: 0.1.2
Author URI: http://www.amcharts.com/

Copyright 2015 amCharts
*/
( function() {
    "use strict";

    /*
    ** CONSTRUCTOR
    */
    function PMG() {
        this.user = {
            buffer: {},
            selectedObject: {},
            leftClick: false,
            pixelized: false,
            lockListener: false,
            lockDragging: false
        };
        this.transition = 400;
        this.defaults = {
            map: "worldLow",
            pixel: "circle",
            types: {
                pixel: "circle",
                pointer: "multiple",
                bucket: "multiple"
            },
            color: "rgba(75,216,181,0.8)",
            landColor: "rgba(129,129,129,1)",
            waterColor: "rgba(80,80,80,1)",
            size: 16,
            rollOverSize: 16,
            distance: 1,
            dashLength: 0,
            arrow: "end"
        };
        this.maps = [];
        this.tool = "pointer";
        this.initialize();
    }

    /*
     ** INITIATE INSTANCE
     */
    PMG.prototype.initialize = function() {
        var _this = this;

        _this.observeKeys();
        _this.observeMouse();
        _this.observeWindow();
        _this.observeWidget( ".map-toolbar", _this.defaults );
        _this.observeWidget( ".app-controls", _this.defaults, function( ref ) {
            if ( ref.type == "color" ) {
                jQuery( ".app" ).css( {
                    backgroundColor: pixelMapper.handleColor( ref.ref.waterColor )
                } );
            }
        } );
        _this.loadMap( "worldLow.js" );
        _this.createCursor();

        jQuery( ".btn" ).each( function() {
            if ( jQuery( this ).hasClass( "btn-generate" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    _this.generatePixel();
                } );

            } else if ( jQuery( this ).hasClass( "btn-export-svg" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    var data = jQuery( this ).data();

                    jQuery( ".app-controls-export span" ).text( data.label );
                    _this.exportSVG();
                } );
            } else if ( jQuery( this ).hasClass( "btn-export-json" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    var data = jQuery( this ).data();

                    jQuery( ".app-controls-export span" ).text( data.label );
                    _this.exportJSON();
                } );
            } else if ( jQuery( this ).hasClass( "btn-export-png" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    var data = jQuery( this ).data();

                    jQuery( ".app-controls-export span" ).text( data.label );
                    _this.exportIMAGE();
                } );
            } else if ( jQuery( this ).hasClass( "btn-sidemenu" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    jQuery( ".app" ).toggleClass( "open" );
                } );
            } else if ( jQuery( this ).hasClass( "btn-export-download" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    var blob = new Blob( [ _this.user.export.content ], {
                        type: _this.user.export.mimeType
                    } );
                    saveAs( blob, "amCharts.pixelMap." + _this.user.export.extension );

                    jQuery( ".btn-toggle" ).trigger( "click" );
                } );

            } else if ( jQuery( this ).hasClass( "btn-export-close" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                    _this.cover( {
                        end: 100
                    } );
                    _this.editor.setValue( new Array( 100 ).join( "\n" ) );
                    jQuery( ".app" ).removeClass( "exporting exporting-fadein" );
                } );

            } else if ( jQuery( this ).hasClass( "btn-toggle" ) ) {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();

                    // CLOSE EXPORTING
                    if ( jQuery( ".app" ).hasClass( "exporting" ) ) {
                        jQuery( ".app" ).removeClass( "exporting exporting-fadein" );
                        jQuery( this ).find( ".ui-label" ).text( "Show options" );
                        setTimeout( function() {
                            jQuery( ".app-controls" ).removeClass( "fx" ).css( {
                                top: "",
                                left: "",
                                overflow: ""
                            } );
                        }, _this.transition );

                        // TOGGLE
                    } else {
                        jQuery( ".app-controls" ).css( {
                            overflow: "hidden"
                        } ).toggleClass( "open" );

                        setTimeout( function() {
                            jQuery( ".app-controls" ).css( {
                                overflow: "visible"
                            } );
                        }, _this.transition );

                        if ( jQuery( ".app-controls" ).hasClass( "open" ) ) {
                            jQuery( this ).find( ".ui-label" ).text( "Hide options" );
                        } else {
                            jQuery( this ).find( ".ui-label" ).text( "Show options" );
                        }
                    }
                } );

                // WE DONT LIKE HASHBANG CHANGES
            } else {
                jQuery( this ).on( "click", function( e ) {
                    e.preventDefault();
                } );
            }
        } );

        // APP CONTROL
        jQuery( ".app-controls" ).draggable( {
            handle: ".grip",
            containment: "parent",
            nawrBottomRightInstead: true,
            start: function() {
                jQuery( ".app-controls" ).removeClass( "fx" );
                jQuery( ".app" ).data( {
                    prevClass: jQuery( ".app" ).attr( "class" )
                } ).addClass( "dragging" );
            },
            using: function( e, ui ) {
                var viewport = {
                    width: jQuery( window ).width(),
                    height: jQuery( window ).height()
                }
                var element = {
                    width: jQuery( ".app-controls" ).width(),
                    height: jQuery( ".app-controls" ).height()
                }
                jQuery( ".app-controls" ).css( {
                    bottom: viewport.height - ( ui.position.top + element.height ),
                    right: viewport.width - ( ui.position.left + element.width )
                } );
            },
            stop: function( e, ui ) {
                var data = jQuery( ".app" ).data();
                jQuery( ".app-controls" ).addClass( "fx" );
                jQuery( ".app-controls" ).css( {
                    width: "",
                    height: ""
                } );
                jQuery( ".app" ).attr( "class", data.prevClass );
            }
        } );

        jQuery( ".app-controls-export textarea" ).each( function() {
            _this.editor = CodeMirror.fromTextArea( this, {
                lineNumbers: true,
                readOnly: true,
                theme: "ambiance",
                mode: "text/html"
            } );
            _this.editor.setValue( new Array( 100 ).join( "\n" ) );
        } );

        // WAIT FOR AMCHARTS
        AmCharts.ready( function() {
            // FLAG IE
            if ( AmCharts.isIE && AmCharts.IEversion < 11 ) {
                jQuery( ".app" ).addClass( "lovely-ie" );
            }
        } );

        // HIDE DOWNLOAD
        if ( saveAs === undefined ) {
            jQuery( ".btn-export-download" ).hide();
        }
        jQuery( window ).trigger( "resize" );
    };

    /*
    ** GENERATES CURSOR IMAGERY
    */
    PMG.prototype.createCursor = function() {
        var _this = this;
        var canvas = new fabric.StaticCanvas( undefined, {
            width: 33,
            height: 33
        } );
        var cursorPath, svg, g, circle;

        // GENERATE CURSOR
        function setCursor( canvasCursor, icoCursor, nativeCursor ) {
            var data;
            var cursors = [];

            if ( canvasCursor ) {
                data = canvas.toDataURL( {
                    format: "png",
                    quality: 1
                } );
                cursors.push( "url(" + data + ")" );
            }

            if ( icoCursor && false ) {
                cursors.push( "url(" + icoCursor + ".ico)" );
            }

            if ( nativeCursor ) {
                cursors.push( nativeCursor );
            } else {
                cursors.push( "auto" );
            }

            jQuery( ".map-container" ).css( {
                cursor: cursors.join( ", " )
            } );
        }

        // POINTER
        if ( _this.tool == "pointer" ) {
            cursorPath = "./static/img/cursor/cursor-pointer";
            fabric.Image.fromURL( cursorPath + ".png", function( img ) {
                img.set( {
                    top: 0,
                    left: 0
                } );
                canvas.add( img );

                setCursor( true, cursorPath );
            } );

            // BUCKET
        } else if ( _this.tool == "bucket" ) {
            svg = jQuery( ".svgMask" ).get( 0 );
            fabric.parseSVGDocument( svg, function( objects, options ) {
                var g = fabric.util.groupSVGElements( objects, options );
                var rgba = _this.defaults.color;

                rgba = [ rgba.r, rgba.g, rgba.b, rgba.a ];
                cursorPath = "./static/img/cursor/cursor-bucket";

                g.set( {
                    top: 16,
                    left: 8,
                    fill: "rgba(" + rgba.join( "," ) + ")"
                } );

                fabric.Image.fromURL( cursorPath + ".png", function( img ) {
                    img.set( {
                        top: 0,
                        left: 0
                    } );
                    canvas.add( img, g );

                    setCursor( true, cursorPath );
                } );
            } );

            // IMAGE
        } else if ( _this.tool == "image" ) {
            svg = jQuery( ".menu-images > a > svg" ).clone( true ).get( 0 );
            fabric.parseSVGDocument( svg, function( objects, options ) {
                var g = fabric.util.groupSVGElements( objects, options );
                var circle = new fabric.Circle( {
                    fill: "#FFFFFF",
                    top: 3,
                    left: 3,
                    radius: 10
                } );
                circle.setShadow( {
                    color: 'rgba(0,0,0,0.3)',
                    blur: 3,
                    offsetY: 1
                } );
                g.set( {
                    scaleX: 0.4,
                    scaleY: 0.4,
                    originX: "center",
                    originY: "center",
                    top: 13,
                    left: 13
                } );
                canvas.add( circle, g );

                setCursor( true, false, "crosshair" );
            } );

            // LINE
        } else if ( _this.tool == "line" ) {
            setCursor( false, false, "crosshair" );

            // TEXT
        } else if ( _this.tool == "text" ) {
            setCursor( false, false, "text" );

            // DEFAULT
        } else {
            setCursor( false, false, "auto" );
        }
    };

    /*
    ** CREATE; OBSERVE ALL FEATURES FOR A POPUP
    */
    PMG.prototype.observeWidget = function( target, ref, cb ) {
        var _this = this;

        jQuery( target )
            .find( "*" )
            .on( "mouseover", function() {
                jQuery( ".app" ).addClass( "disable-cursor" );
            } )
            .on( "mouseleave", function() {
                jQuery( ".app" ).removeClass( "disable-cursor" );
            } );

        /* MENU; STATE */
        jQuery( target )
            .on( "mouseover", function() {
                jQuery( target ).addClass( "active-menu" );
            } )
            .on( "mouseleave", function() {
                jQuery( target ).removeClass( "active-menu" );
            } )
            .find( "li" )
            .on( "mouseover", function() {
                if ( !jQuery( this ).parents( "li" ).hasClass( "open" ) ) {
                    jQuery( target ).find( "li" ).removeClass( "open" );
                }

                if ( !jQuery( this ).hasClass( "expandable" ) ) {
                    return false;
                }

                if ( !_this.user.pixelized && ( jQuery( this ).hasClass( "menu-pointer" ) || jQuery( this ).hasClass( "menu-bucket" ) ) ) {
                    return false;
                }

                jQuery( this ).addClass( "open" );

                if ( jQuery( target ).hasClass( "app-controls" ) ) {
                    jQuery( this ).find( "> ul" ).position( {
                        of: this,
                        my: "left top",
                        at: "left bottom"
                    } );
                } else {
                    jQuery( this ).find( "> ul" ).position( {
                        of: this,
                        my: "left top",
                        at: "right+3 top",
                        within: ".app-map"
                    } );
                }
            } )
            .on( "mouseleave", function() {
                var item = this;
                if ( !jQuery( item ).hasClass( "lock" ) ) {
                    jQuery( item ).removeClass( "open" );
                }
            } );


        if ( jQuery( target ).hasClass( "draggable" ) ) {

            /* WIDGET; DRAGGABLE */
            jQuery( target ).draggable( {
                handle: ".grip",
                containment: "parent",
                start: function() {
                    jQuery( target ).removeClass( "fx" );
                    jQuery( ".app" ).data( {
                        prevClass: jQuery( ".app" ).attr( "class" )
                    } ).addClass( "dragging" );
                    if ( jQuery( target ).hasClass( "map-popup" ) ) {
                        jQuery( target ).addClass( "dragged" );
                    }
                },
                stop: function( e, ui ) {
                    var data = jQuery( ".app" ).data();
                    jQuery( ".app" ).attr( "class", data.prevClass );
                    jQuery( target ).addClass( "fx" );
                }
            } );
            jQuery( target ).resizable();
        }

        // COLOR PICKER
        jQuery( target ).find( ".control-colorpicker" ).each( function() {
            var control = this;
            var data = jQuery( this ).data();
            var dad = jQuery( this ).parents( "li" );
            var dadInput = dad.find( ".fill-color" );

            jQuery( this ).spectrum( {
                flat: true,
                showAlpha: true,
                cancelText: false,
                showButtons: false,
                preferredFormat: "rgb",
                color: data.init || ref.color || "rgba(75,216,181,0.8)",
                show: function( cp ) {
                    dad.find( ".sp-slider" ).addClass( "fill-color-flat" );
                    dadInput.on( "keyup", function() {
                        var color = tinycolor.stringInputToObject( this.value );

                        clearTimeout( control.timer );
                        if ( color ) {
                            jQuery( this ).removeClass( "error" );
                            control.timer = setTimeout( function() {
                                _this.updateColor( cp, ref, dad, control, color );
                            }, 1500 );
                        } else {
                            jQuery( this ).addClass( "error" );
                        }
                    } ).on( "blur", function() {
                        _this.updateColor( cp, ref, dad, control );
                    } );

                    _this.updateColor( cp, ref, dad, control );
                },
                move: function( cp ) {
                    _this.updateColor( cp, ref, dad, control );

                    if ( cb ) {
                        cb.apply( _this, [ {
                            type: "color",
                            ref: ref
                        } ] );
                    }
                }
            } ).on( "dragstart.spectrum", function() {
                jQuery( ".app" ).addClass( "dragging" );
                jQuery( this ).parents( "li.open" ).addClass( "lock" );
            } ).on( "dragstop.spectrum", function() {
                jQuery( ".app" ).removeClass( "dragging" );
                jQuery( this ).parents( "li.open" ).removeClass( "lock" );
            } );

        } );

        // COLOR PICKER
        jQuery( target ).find( ".map-popup-title" ).each( function() {
            var data = jQuery( this ).data();
            jQuery( this ).on( "keyup", function() {
                ref[ data.type ] = this.value;

                if ( cb ) {
                    cb.apply( _this, [ {
                        type: "text",
                        ref: ref
                    } ] );
                }
            } ).on( "focus", function() {
                _this.user.lockListener = true;
            } ).on( "blur", function() {
                _this.user.lockListener = false;
            } ).val( ref[ data.type ] );


            if ( pixelMapper.selectedObject.pixelMapperText ) {
                this.focus();
                jQuery( this ).select();
            }
        } );

        // SLIDER
        jQuery( target ).find( ".control-slider" ).each( function() {
            var data = jQuery( this ).data();
            var label = jQuery( this ).find( "small" );
            var container = jQuery( this ).find( "> div" );

            jQuery( container ).slider( {
                range: "min",
                min: Number( data.min ),
                max: Number( data.max ),
                step: Number( data.step ),
                value: ref[ data.type ] || Number( data.init ),
                create: function( event, ui ) {
                    label.text( data.init + "px" );
                    ref[ data.type ] = ref[ data.type ] || Number( data.init );
                },
                slide: function( event, ui ) {
                    label.text( ui.value + "px" );
                    ref[ data.type ] = ui.value;
                    if ( cb ) {
                        cb.apply( _this, [ {
                            type: "slider",
                            ref: ref
                        } ] );
                    }
                }
            } );
        } );

        // IMAGES
        jQuery( target ).find( ".menu-images" ).each( function() {
            var ul = jQuery( this ).find( "ul" );
            var parent = jQuery( this );
            var icons = jQuery( ".icon-list > svg" );
            jQuery( new Array( 28 ) ).each( function( id ) {
                var li = jQuery( "<li>" ).appendTo( ul );
                var a = jQuery( "<a>" ).appendTo( li );
                var svgPath = jQuery( icons[ id ] ).find( "path" ).attr( "d" );

                a.attr( {
                    href: "#"
                } ).append( icons[ id ] );

                li.on( "click", function( e ) {
                    e.preventDefault();
                    jQuery( this ).parents( ".menu-images" ).data( {
                        svgPath: svgPath,
                        id: id
                    } ).find( "> a > svg > path" ).attr( {
                        d: svgPath
                    } );
                    ref.svgPath = svgPath;
                    ref.image_id = id;
                } ).data( {
                    type: "image",
                    svgPath: svgPath,
                    id: id
                } );

                // DEFAULT
                if ( id === 0 ) {
                    parent.data( {
                        svgPath: svgPath,
                        id: id
                    } );
                    ref.svgPath = svgPath;
                    ref.image_id = id;
                }
            } );
        } );


        // PIXEL TYPES
        jQuery( target ).find( ".control-type" ).each( function() {
            var data1 = jQuery( this ).data();

            // DEFAULT
            if ( ref[ data1.prop ] == data1.type ) {
                jQuery( this ).addClass( "active" );
            }

            // ON CLICK
            jQuery( this ).on( "click", function( e ) {
                e.preventDefault();
                var data = jQuery( this ).data();
                var value = data.type;

                jQuery( this ).parents( ".control-types" ).find( ".control-type" ).removeClass( "active" );

                // ARROW TOGGLE
                if ( data.prop == "arrow" && ref.arrow == value ) {
                    value = "none";
                    ref.arrow = value;
                } else {
                    jQuery( this ).addClass( "active" );
                }

                if ( ref.types ) {
                    ref.types.pixel = value;
                }

                ref[ data.prop ] = value;

                if ( cb ) {
                    cb.apply( _this, [ {
                        type: "type",
                        ref: ref
                    } ] );
                }
            } );
        } );

        // PROPERTY TOOLS
        jQuery( target ).find( ".headline .prop-tool" ).each( function() {
            var headline = jQuery( this ).parent().find( ".label" );

            jQuery( this )
                .on( "click", function( e ) {
                    e.preventDefault();
                    clearTimeout( this.timer );
                } )
                .on( "mouseover", function() {
                    clearTimeout( this.timer );
                } )
                .on( "mouseleave", function() {
                    clearTimeout( this.timer );
                } );

            if ( jQuery( this ).hasClass( "remove" ) ) {
                jQuery( this )
                    .on( "click", function( e ) {
                        e.preventDefault();

                        pixelMapper.deleteObject();
                        _this.removePopups();
                        jQuery( ".app" ).removeClass( "disable-cursor" );
                    } )
                    .on( "mouseover", function() {
                        jQuery( headline ).text( "Remove object" );
                    } )
                    .on( "mouseleave", function() {
                        jQuery( headline ).text( headline.data( "label" ) );
                    } );

            } else if ( jQuery( this ).hasClass( "lock" ) ) {
                var icon = jQuery( this ).find( "i" );
                jQuery( this )
                    .on( "click", function() {
                        var data = jQuery( this ).data();

                        ref.locked = !ref.locked;

                        if ( ref.locked ) {
                            jQuery( icon ).attr( "class", "icon-prop-unpin" );
                            jQuery( headline ).text( "Unpin position" );
                        } else {
                            jQuery( icon ).attr( "class", "icon-prop-pin" );
                            jQuery( headline ).text( "Pin position" );
                        }

                        if ( cb ) {
                            cb.apply( _this, [ {
                                type: "lock",
                                ref: ref
                            } ] );
                        }
                    } )
                    .on( "mouseover", function() {
                        jQuery( headline ).text( ref.locked ? "Unpin position" : "Pin position" );
                    } )
                    .on( "mouseleave", function() {
                        jQuery( headline ).text( headline.data( "label" ) );
                    } );

                if ( !ref.locked ) {
                    jQuery( icon ).attr( "class", "icon-prop-pin" );
                }

            }
        } );

        // DROPDOWN
        jQuery( target ).find( ".control-dropdown select" ).val( "worldLow.js" ).combobox( {
            select: function( event, data ) {
                _this.loadMap( data.item.value );
            }
        } );

        // TOOLBAR
        if ( jQuery( target ).hasClass( "map-toolbar" ) ) {
            jQuery( target ).find( "li" ).on( "click", function( e ) {
                var i1, i2;
                var data = jQuery( this ).data();
                var parent = jQuery( this ).parents( ".expandable" );
                var types = {
                    areas: 0,
                    images: 0,
                    lines: 0
                };

                e.stopPropagation();
                e.preventDefault();

                // REMOVE CLASSES
                jQuery( "li" ).removeClass( "active" );
                jQuery( ".app" ).removeClass( "active-tool tool-pointer tool-bucket tool-line tool-text tool-image" );

                // SELECT IF DIFFERET
                if ( _this.tool != data.type || parent.length ) {
                    _this.tool = data.type;

                    // SELECT TYPE
                    if ( jQuery( this ).hasClass( "select-type" ) ) {
                        jQuery( parent ).removeClass( "menu-single menu-multiple" );
                        jQuery( parent ).addClass( "menu-" + data.value ).data( {
                            value: data.value
                        } );

                        if ( data.type == "pointer" ) {
                            pixelMapper.selectTool( "select", !_this.user.pixelized || data.value == "multiple" );
                        } else if ( data.type == "bucket" ) {
                            pixelMapper.selectTool( "paint", !_this.user.pixelized || data.value == "multiple" );
                        }

                        ref.types[ data.type ] = data.value;

                        // LINE
                    } else if ( data.type == "line" ) {
                        pixelMapper.selectTool( "line", false );

                        // TEXT
                    } else if ( data.type == "text" ) {
                        pixelMapper.selectTool( "text", false );

                        // NONE
                    } else {
                        pixelMapper.selectTool( false, false );
                    }

                    // IGNORE COLOR
                    if ( !jQuery( parent ).hasClass( "menu-color" ) ) {
                        // TOGGLE CLASS
                        if ( _this.tool ) {
                            jQuery( ".app" ).addClass( "active-tool tool-" + _this.tool );
                            jQuery( this ).addClass( "active" );
                        }

                        // TOGGLE CLASSES
                        if ( !jQuery( this ).hasClass( "expandable" ) ) {
                            jQuery( parent ).addClass( "active" ).removeClass( "open" );
                            jQuery( ".app" ).removeClass( "active-menu" );
                        }

                        _this.createCursor();
                    }

                    // NONE; DESELECT
                } else {
                    _this.tool = false;
                    pixelMapper.selectTool( false, false );

                    jQuery( ".menu-pointer" ).first().trigger( "click" );
                    return;
                }

                // TOGGLE SELECTABILITY
                for ( i1 in types ) {
                    if ( i1 ) {
                        for ( i2 = 0; i2 < _this.map.dataProvider[ i1 ].length; i2++ ) {
                            _this.map.dataProvider[ i1 ][ i2 ].selectable = [ "bucket", "pointer" ].indexOf( _this.tool ) != -1;
                        }
                    }
                }

                // REMOVE PREVIOUS
                _this.removePopups();
                _this.user.lockListener = false;
            } );
        } else {
            _this.user.selectedObject = ref;
        }
    };

    /*
     ** EXPORT TO SVG
     */
    PMG.prototype.exportSVG = function() {
        var _this = this;
        var clone = jQuery( _this.map.container.container ).clone();
        var div = jQuery( "<div>" ).append( clone );
        var output = "";

        // ADD COPYRIGHT
        jQuery( "<desc>" )
            .text( "This map was created using Pixel Map Generator by amCharts and is licensed under the Creative Commons Attribution 4.0 International License.\nYou may use this map the way you see fit as long as proper attribution to the name of amCharts is given in the form of link to http://pixelmap.amcharts.com/\nTo view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/\n\nIf you would like to use this map without any attribution, you can acquire a commercial license for the JavaScript Maps - a tool that was used to produce this map.\nTo do so, visit amCharts Online Store: http://www.amcharts.com/online-store/" )
            .insertAfter( jQuery( clone ).find( "desc" ) );

        // REMOVE AREAS IF PIXELIZED
        if ( _this.user.pixelized ) {
            jQuery( div ).find( ".amcharts-map-area" ).remove();
        }

        // REMOVE ZOOM
        jQuery( clone )
            .attr( {
                style: null,
                xmlns: "http://www.w3.org/2000/svg",
                "xmlns:xlink": "http://www.w3.org/1999/xlink",
                width: _this.map.divRealWidth,
                height: _this.map.divRealHeight,
                "xml:space": "preserve"
            } )
            .find( ".amcharts-zoom-control" )
            .remove();

        // CONVERT RGBA TO HEX / OPACITY (ILLUSTRATOR SUPPORT)
        jQuery( clone ).find( "*" ).each( function() {
            var item = this;
            jQuery( [ "fill", "stroke" ] ).each( function() {
                var name = this;
                var value = jQuery( item ).attr( name );
                if ( value ) {
                    var rgba = tinycolor( value );
                    var hex = rgba.toHex();
                    var alpha = rgba.getAlpha();

                    jQuery( item ).attr( name, "#" + hex );
                    jQuery( item ).attr( name + "-opacity", alpha );
                }
            } );

            if ( this.tagName == "g" ) {
                var children = jQuery( this ).find( "*" );
                if ( children.length === 0 ) {
                    jQuery( this ).remove();
                }
            }
        } );

        // SET BACKGROUND COLOR
        jQuery( div ).find( ".amcharts-bg" ).attr( {
            fill: "#" + tinycolor( _this.defaults.waterColor ).toHex(),
            "fill-opacity": tinycolor( _this.defaults.waterColor ).getAlpha()
        } );

        // ADD ABRAND
        jQuery( ".app-brand-logo > g" ).clone().attr( {
            transform: "translate(20,20)"
        } ).appendTo( clone );

        _this.cover( {
            text: "Exporting...",
            start: 0,
            end: 90
        } );
        setTimeout( function() {
            var output = div.html();
            output = output.replace( /<g><\/g>/g, "" );
            output = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" + output;
            output = html_beautify( output, {
                wrap_line_length: 99999
            } );

            _this.user.export = {
                content: output,
                mimeType: "text/xml",
                extension: "svg"
            };

            // PROCESS DONE SHOW RESULTS
            _this.cover( {
                end: 100,
                complete: function() {
                    var pos = jQuery( ".app-controls" ).position();
                    jQuery( ".app-controls" ).removeClass( "fx" ).css( pos );

                    // ABOVE THE STACK
                    setTimeout( function() {
                        jQuery( ".app-controls" ).addClass( "fx" );
                        jQuery( ".app" ).addClass( "exporting" );
                    }, 1 );

                    setTimeout( function() {
                        jQuery( ".app-controls .btn-toggle .ui-label" ).text( "Close" );
                        _this.editor.setValue( output );
                        jQuery( ".app" ).addClass( "exporting-fadein" );
                    }, _this.transition );
                }
            } );
        }, _this.transition );
    };

    /*
    ** EXPORT TO PNG
    ** https://github.com/amcharts/export
    */
    PMG.prototype.exportIMAGE = function() {
        var _this = this;
        var ame = new AmCharts.export( _this.map, {} );

        _this.cover( {
            text: "Exporting...",
            start: 0,
            end: 90
        } );

        ame.capture( {
            backgroundColor: pixelMapper.handleColor( this.defaults.waterColor ),

            // SIMPLY HIDE ZOOM CONTROLS
            reviver: function(obj) {
                if ( obj.className && obj.className.indexOf("zoom") != -1 ) {
                    obj.opacity = 0;
                }
            },

            // INJECT BRAND LOGO
            afterCapture: function() {
                var instance = this;
                var svg = jQuery( ".app-brand-logo" ).get( 0 );
                fabric.parseSVGDocument( svg, function( objects, options ) {
                    var brand = fabric.util.groupSVGElements( objects, options );

                    // POSITION; ADD
                    brand.set( {
                        top: 20,
                        left: 20
                    } );
                    instance.setup.fabric.add( brand );

                    // GENERATE IMAGE; HIDE COVER; DOWNLOAD
                    instance.toPNG( {}, function( data ) {

                        _this.cover( {
                            end: 100,
                            complete: function() {
                                instance.download( data, "image/png", "amcharts.pixelMap.png" );
                                jQuery( ".app" ).removeClass( "exporting exporting-fadein" );
                            }
                        } );
                    } );
                } );
            }
        } );
    };

    /*
     ** EXPORT TO SVG
     */
    PMG.prototype.exportJSON = function() {
        var _this = this;
        var i1, i2;
        var cache = [];
        var item, tmp;
        var output = "";
        var colors = {
            landColor: pixelMapper.handleColor( _this.defaults.landColor ),
            waterColor: pixelMapper.handleColor( _this.defaults.waterColor )
        };
        var config = {
            type: "map",
            pathToImages: "http://www.amcharts.com/lib/3/images/",
            addClassNames: true,
            fontSize: 15,

            color: _this.defaults.brandHEX,

            backgroundAlpha: 1,
            backgroundColor: colors.waterColor,

            dataProvider: {
                map: _this.map.dataProvider.map,
                getAreasFromMap: true,
                images: [ {
                    top: 40,
                    left: 60,
                    width: 80,
                    height: 40,
                    pixelMapperLogo: true,
                    imageURL: _this.defaults.brandSVG,
                    url: "http://www.amcharts.com"
                } ]
            },

            balloon: {
                horizontalPadding: 15,
                borderAlpha: 0,
                borderThickness: 1,
                verticalPadding: 15
            },

            areasSettings: {
                color: colors.landColor,
                outlineColor: colors.waterColor,
                rollOverOutlineColor: colors.waterColor,
                rollOverBrightness: 20,
                selectedBrightness: 20,
                selectable: true,
                unlistedAreasAlpha: 0,
                unlistedAreasOutlineAlpha: 0
            },

            imagesSettings: {
                alpha: 1,
                color: colors.landColor,
                outlineAlpha: 0,
                rollOverOutlineAlpha: 0,
                outlineColor: colors.waterColor,
                rollOverBrightness: 20,
                selectedBrightness: 20,
                selectable: true
            },

            linesSettings: {
                color: colors.landColor,
                selectable: true,
                rollOverBrightness: 20,
                selectedBrightness: 20
            },

            zoomControl: {
                zoomControlEnabled: true,
                homeButtonEnabled: false,
                panControlEnabled: false,

                right: 38,
                bottom: 30,

                minZoomLevel: 0.25,

                gridHeight: 100,
                gridAlpha: 0.1,
                gridBackgroundAlpha: 0,
                gridColor: "#FFFFFF",
                draggerAlpha: 1,
                buttonCornerRadius: 2
            }
        };

        // AREAS; UNPIXELIZED
        if ( !_this.user.pixelized && _this.map.dataProvider.areas.length ) {
            for ( i1 = 0; i1 < _this.map.dataProvider.areas.length; i1++ ) {
                item = _this.map.dataProvider.areas[ i1 ];
                tmp = {
                    id: item.id,
                    groupId: item.groupId,
                    title: item.title,
                    color: item.color
                };
                if ( item.pixelMapperModified ) {
                    if ( !config.dataProvider.areas ) {
                        config.dataProvider.areas = [];
                    }
                    config.dataProvider.areas.push( tmp );
                }
            }

            // AREAS; PIXELIZED
        } else {
            config.areasSettings.alpha = 0;
            config.areasSettings.outlineAlpha = 0;
            config.dataProvider.getAreasFromMap = false;
        }

        // IMAGES
        if ( _this.map.dataProvider.images.length ) {
            for ( i1 = 0; i1 < _this.map.dataProvider.images.length; i1++ ) {
                item = _this.map.dataProvider.images[ i1 ];
                tmp = {
                    id: item.id,
                    groupId: item.groupId,
                    selectable: true,
                    title: item.title
                };

                // POSITION
                if ( item.latitude && item.longitude ) {
                    tmp.longitude = item.longitude;
                    tmp.latitude = item.latitude;
                } else {
                    tmp.top = item.top;
                    tmp.left = item.left;
                }

                // PIXEL
                if ( item.pixelMapperPixel ) {
                    tmp.type = item.type;
                    tmp.color = item.color;
                    tmp.width = item.width;
                    tmp.height = item.height;
                    tmp.scale = item.scale;
                    tmp.fixedSize = false;

                    if ( item.type == "diamond" ) {
                        tmp.rotation = item.rotation;
                    }

                    // TEXT
                } else if ( item.pixelMapperText ) {
                    tmp.label = item.label;
                    tmp.labelPosition = item.labelPosition;
                    tmp.labelColor = item.labelColor;
                    tmp.labelRollOverColor = item.labelRollOverColor;
                    tmp.labelFontSize = item.labelFontSize;

                    // IMAGE
                } else if ( item.pixelMapperImage ) {
                    tmp.svgPath = item.svgPath;
                    tmp.color = item.color;
                    tmp.width = item.width;
                    tmp.height = item.height;
                    tmp.scale = item.scale;
                }

                config.dataProvider.images.push( tmp );
            }
        }

        // LINES
        if ( _this.map.dataProvider.lines.length ) {
            config.dataProvider.lines = [];
            for ( i1 = 0; i1 < _this.map.dataProvider.lines.length; i1++ ) {
                item = _this.map.dataProvider.lines[ i1 ];
                tmp = {
                    id: item.id,
                    arc: item.arc,
                    selectable: true,
                    color: item.color,
                    thickness: item.thickness,
                    dashLength: item.dashLength,
                    longitudes: item.longitudes,
                    latitudes: item.latitudes
                };

                // ARROW
                if ( item.arrow ) {
                    tmp.arrow = item.arrow;
                    tmp.arrowSize = item.arrowSize;
                }

                config.dataProvider.lines.push( tmp );
            }
        }

        _this.cover( {
            text: "Exporting...",
            start: 0,
            end: 90
        } );
        setTimeout( function() {
            output = JSON.stringify( config, function( key, value ) {
                if ( typeof value === 'object' && value !== null ) {
                    if ( cache.indexOf( value ) !== -1 ) {
                        return;
                    }
                    cache.push( value );
                }
                return value;
            }, "\t" );

            output = output.replace( /\n/g, "\n\t\t\t\t" );
            output = jQuery( ".export-json" )
                .val()
                .replace( /\{\{CONFIG\}\}/g, output )
                .replace( /\{\{HEIGHT\}\}/g, jQuery( ".map-container" ).height() )
                .replace( /\{\{FILE\}\}/g, _this.user.loadMap )
                .replace( /\{\{LANDCOLOR\}\}/g, colors.landColor )
                .replace( /\{\{WATERCOLOR\}\}/g, colors.waterColor )
                .replace( /\{\{CONTRASTCOLOR\}\}/g, _this.defaults.brandHEX );

            _this.user.export = {
                content: output,
                mimeType: "text/plain",
                extension: "html"
            };

            // PROCESS DONE SHOW RESULTS
            _this.cover( {
                end: 100,
                complete: function() {
                    var pos = jQuery( ".app-controls" ).position();
                    jQuery( ".app-controls" ).removeClass( "fx" ).css( pos );

                    // ABOVE THE STACK
                    setTimeout( function() {
                        jQuery( ".app-controls" ).addClass( "fx" );
                        jQuery( ".app" ).addClass( "exporting" );
                    }, 1 );

                    setTimeout( function() {
                        jQuery( ".app-controls .btn-toggle .ui-label" ).text( "Close" );
                        _this.editor.setValue( output );
                        jQuery( ".app" ).addClass( "exporting-fadein" );
                    }, _this.transition );
                }
            } );
        }, _this.transition );
    };

    /*
    ** REMOVE POPUPS
    */
    PMG.prototype.removePopups = function() {
        var _this = this;
        var popups = jQuery( ".app-map .map-popup" );

        popups.each( function() {
            var position = jQuery( this ).position();

            jQuery( this ).css( {
                top: position.top + 50,
                opacity: 0
            } );
        } );
        setTimeout( function() {
            popups.remove();
        }, _this.transition );
    };

    /*
    ** GENERATE PIXELS
    */
    PMG.prototype.generatePixel = function() {
        var _this = this;
        jQuery( ".app" ).addClass( "generating" );
        _this.removePopups();
        _this.cover( {
            text: "Generating...",
            start: 0,
            end: 0.1
        } );

        setTimeout( function() {
            pixelMapper.build();
            _this.user.pixelized = true;

            jQuery( ".map-toolbar > ul > li.menu-pointer" ).addClass( "menu-multiple" );
            jQuery( ".map-toolbar > ul > li.menu-bucket" ).addClass( "menu-multiple" );
        }, _this.transition );
    };

    /*
     ** ADAPT AUTOCOMPLETE ON VIEWPORT
     */
    PMG.prototype.autoCompleteViewport = function() {
        jQuery( ".ui-autocomplete" ).css( {
            maxHeight: jQuery( ".app" ).height() / 2
        } );
    };

    /*
    ** OBSERFVE WINDOW
    */
    PMG.prototype.observeWindow = function() {
        var _this = this;
        jQuery( window ).on( "resize", _this.autoCompleteViewport );
        jQuery( window ).on( "resize", function( e ) {
            var viewport = {
                width: jQuery( window ).width(),
                height: jQuery( window ).height()
            }

            if ( viewport.width < 1200 ) {
                jQuery( ".app-controls" ).addClass( "portrait" );
            } else {
                jQuery( ".app-controls" ).removeClass( "portrait" );
            }

            jQuery( ".app-controls" ).position( {
                of: ".app-map",
                my: "right bottom",
                at: "right bottom",
                within: ".app-map",
                using: function( pos, cfg ) {
                    var viewport = {
                        width: jQuery( window ).width(),
                        height: jQuery( window ).height()
                    }

                    jQuery( cfg.element.element ).css( {
                        bottom: viewport.height - ( pos.top + cfg.element.height ) + 20,
                        right: viewport.width - ( pos.left + cfg.element.width ) + 20
                    } ).addClass( "fx" );
                }
            } );
        } );
    };

    /*
    ** UPDATE COLOR CHANGES
    */
    PMG.prototype.updateColor = function( cp, ref, target, control, RAW ) {
        var _this = this;
        var data = jQuery( control ).data();
        var color = cp.toRgb();
        var colorFlat = tinycolor.fromRatio( {
            h: cp._originalInput.h,
            s: 1,
            v: 1
        } );
        var rgba = pixelMapper.handleColor( color );
        var rgbFlat = colorFlat.toRgbString();
        var hsv = cp.toHsv();

        // UPDATE AREAS / PIXELS
        if ( data.type == "waterColor" || data.type == "landColor" ) {
            ref[ data.type ] = color;

            if ( data.type == "waterColor" ) {
                if ( hsv.v < 0.5 ) {
                    jQuery( ".app-brand" ).removeClass( "dark" );
                    jQuery( ".app-brand-logo-white" ).attr( "fill", "#FFFFFF" );
                    _this.defaults.brandSVG = "http://pixelmap.amcharts.com/static/img/logo.svg";
                    _this.defaults.brandHEX = "#FFFFFF";
                } else {
                    jQuery( ".app-brand" ).addClass( "dark" );
                    jQuery( ".app-brand-logo-white" ).attr( "fill", "#000000" );
                    _this.defaults.brandSVG = "http://pixelmap.amcharts.com/static/img/logo-black.svg";
                    _this.defaults.brandHEX = "#000000";
                }
            }

            if ( _this.map ) {
                var landColor = pixelMapper.handleColor( _this.defaults.landColor );
                var waterColor = pixelMapper.handleColor( _this.defaults.waterColor );
                var rollOverColor = AmCharts.adjustLuminosity( landColor, 0.15 );
                var selectedColor = AmCharts.adjustLuminosity( landColor, 0.30 );
                var outlineColor = waterColor;
                var rollOverOutlineColor = waterColor;

                if ( _this.user.pixelized ) {
                    jQuery( _this.map.dataProvider.images ).each( function() {
                        if ( this.pixelMapperPixel && !this.pixelMapperModified ) {
                            this.image.setAttr( "fill", landColor );

                            this.color = landColor;
                            this.colorReal = landColor;
                            this.rollOverColor = rollOverColor;
                            this.rollOverColorReal = rollOverColor;
                            this.selectedColor = selectedColor;
                            this.selectedColorReal = selectedColor;
                            //this.outlineColor = outlineColor;
                            this.outlineColorReal = outlineColor;
                            //this.rollOverOutlineColor = rollOverOutlineColor;
                            this.rollOverOutlineColorReal = rollOverOutlineColor;

                            _this.map.areasSettings.outlineColor = outlineColor;
                            _this.map.areasSettings.rollOverOutlineColor = rollOverOutlineColor;
                        }
                    } );

                } else {
                    jQuery( _this.map.dataProvider.areas ).each( function() {
                        if ( !this.pixelMapperModified ) {
                            this.displayObject.setAttr( "fill", landColor );
                            this.displayObject.setAttr( "stroke", waterColor );

                            this.color = landColor;
                            this.colorReal = landColor;
                            this.rollOverColor = rollOverColor;
                            this.rollOverColorReal = rollOverColor;
                            this.selectedColor = selectedColor;
                            this.selectedColorReal = selectedColor;
                            //this.outlineColor = outlineColor;
                            this.outlineColorReal = outlineColor;
                            //this.rollOverOutlineColor = rollOverOutlineColor;
                            this.rollOverOutlineColorReal = rollOverOutlineColor;

                            _this.map.areasSettings.outlineColor = outlineColor;
                            _this.map.areasSettings.rollOverOutlineColor = rollOverOutlineColor;
                        }
                    } );
                }
            }

            // INDIVIDUAL COLORS
        } else {
            ref[ data.type ] = color;
            _this.defaults.color = color;

            // UPDATE TOOLBAR
            if ( !jQuery( target ).hasClass( "map-toolbar" ) ) {
                jQuery( ".map-toolbar" ).find( ".control-colorpicker" ).spectrum( "set", rgba );
                jQuery( ".map-toolbar" ).find( ".fill-icon" ).css( {
                    backgroundColor: rgba
                } );
                jQuery( ".map-toolbar" ).find( ".fill-color-flat" ).css( {
                    backgroundColor: rgbFlat
                } );
            }
        }

        jQuery( target ).find( ".fill-icon" ).css( {
            backgroundColor: rgba
        } );

        jQuery( target ).find( ".fill-icon-mask" ).css( {
            fill: rgba
        } );

        if ( RAW ) {
            jQuery( control ).spectrum( "set", RAW );
            cp = tinycolor( RAW );
            _this.updateColor( cp, ref, target, control );
        } else {
            jQuery( target ).find( ".fill-color" ).val( rgba );
        }

        jQuery( target ).find( ".fill-color-flat" ).css( {
            backgroundColor: rgbFlat
        } );

        if ( _this.tool == "bucket" ) {
            _this.createCursor();
        }
    };

    /*
     ** CREATE POPUP
     */
    PMG.prototype.createPopup = function( mapObject ) {
        var _this = this;
        var target = mapObject.displayObject.node;
        var popup = null;

        if ( mapObject.pixelMapperLine ) {
            popup = jQuery( ".template.widget-line .widget" ).clone();
        } else if ( mapObject.pixelMapperImage ) {
            popup = jQuery( ".template.widget-image .widget" ).clone();
        } else if ( mapObject.pixelMapperText ) {
            popup = jQuery( ".template.widget-text .widget" ).clone();
        } else if ( mapObject.pixelMapperPixel ) {
            popup = jQuery( ".template.widget-pixel .widget" ).clone();
        } else if ( mapObject.pixelMapperArea ) {
            popup = jQuery( ".template.widget-area .widget" ).clone();
        }

        // REMOVE PREVIOUS
        _this.removePopups();

        // ADD CLONE; document width - target width - margin
        if ( popup ) {
            popup.css( {
                opacity: 0,
                top: 0,
                left: jQuery( document ).width() - 208 - 20
            } );
            popup.appendTo( ".app-map" );
            popup.position( {
                of: ".app-map",
                my: "right top",
                at: "right-20 top+85",
                within: ".app-map"
            } );
            popup.css( {
                opacity: 1
            } );
            _this.observeWidget( popup, {
                locked: mapObject.locked === undefined ? true : mapObject.locked,
                color: mapObject.color,
                labelColor: mapObject.labelColor,
                labelFontSize: mapObject.labelFontSize || 15,
                title: mapObject.title,
                label: mapObject.label,
                scale: mapObject.scale,
                arrow: mapObject.arrow,
                arrowSize: mapObject.arrowSize,
                dashLength: mapObject.dashLength,
                thickness: mapObject.thickness,
                arc: mapObject.arc,
                pixel: mapObject.pixel || _this.defaults.pixel
            }, function( ev ) {
                _this.user.selectedObject = ev.ref;
                pixelMapper.map.zoomDuration = 0;
                pixelMapper.updateObject();
                pixelMapper.map.zoomDuration = 1;
            } );
        }
    };

    /*
     ** ADD KEY LISTENER
     */
    PMG.prototype.observeKeys = function() {
        var _this = this;

        jQuery( document ).on( "keyup", function( e ) {
            var parent;

            if ( _this.user.lockListener ) {
                return;
            }

            // DELETE
            if ( e.keyCode == 8 || e.keyCode == 46 ) {
                e.preventDefault();
                if ( pixelMapper.selectedObject ) {
                    pixelMapper.deleteObject();
                }

                // ESCAPE
            } else if ( e.keyCode == 27 ) {
                _this.user.lockListener = false;
                _this.map.selectObject();

                _this.removePopups();

                // V; POINTER
            } else if ( e.keyCode == 86 ) {
                parent = jQuery( ".map-toolbar > ul > li.menu-pointer" );

                if ( !_this.user.pixelized ) {
                    jQuery( parent ).trigger( "click" );
                    return false;
                }

                // TOGGLE
                jQuery( parent ).find( "li" ).each( function() {
                    var data = jQuery( this ).data();

                    // TOGGLE
                    if ( _this.tool == "pointer" ) {
                        if ( _this.defaults.types.pointer != data.value ) {
                            jQuery( this ).trigger( "click" );
                            return false;
                        }

                        // SELECT
                    } else if ( _this.defaults.types.pointer == data.value ) {
                        jQuery( this ).trigger( "click" );
                        return false;
                    }
                } );

                // G; BUCKET
            } else if ( e.keyCode == 71 ) {
                parent = jQuery( ".map-toolbar > ul > li.menu-bucket" );

                if ( !_this.user.pixelized ) {
                    jQuery( parent ).trigger( "click" );
                    return false;
                }

                // TOGGLE
                jQuery( ".map-toolbar li" ).removeClass( "active" );
                jQuery( parent ).find( "li" ).each( function() {
                    var data = jQuery( this ).data();

                    // TOGGLE
                    if ( _this.tool == "bucket" ) {
                        if ( _this.defaults.types.bucket != data.value ) {
                            jQuery( this ).trigger( "click" );
                            return false;
                        }

                        // SELECT
                    } else if ( _this.defaults.types.bucket == data.value ) {
                        jQuery( this ).trigger( "click" );
                        return false;
                    }
                } );

                // I; IMAGE
            } else if ( e.keyCode == 73 ) {
                jQuery( ".map-toolbar li.menu-images" ).trigger( "click" );

                // L; LINE
            } else if ( e.keyCode == 76 ) {
                jQuery( ".map-toolbar li.menu-line" ).trigger( "click" );

                // T; TEXT
            } else if ( e.keyCode == 84 ) {
                jQuery( ".map-toolbar li.menu-text" ).trigger( "click" );
            }
        } );
    };

    /*
     ** ADD MOUSE LISTENER
     */
    PMG.prototype.observeMouse = function() {
        var _this = this;
        return;
        jQuery( document ).on( "mousedown touchstart", function( e ) {
            if ( _this.map ) {
                pixelMapper.saveZoom();
            }
            _this.isPressed( e.originalEvent );
        } );

        jQuery( document ).on( "mouseup touchend", function( e ) {
            _this.user.leftClick = false;

            if ( _this.user.mapObject ) {
                if ( _this.user.mapObject.pixelMapperDragging ) {
                    if ( _this.user.mapObject.locked ) {
                        _this.user.mapObject.fixToStage();
                        _this.user.mapObject.locked = false;
                    } else {
                        _this.user.mapObject.fixToMap();
                        _this.user.mapObject.locked = true;
                    }
                }
                _this.user.mapObject.pixelMapperDraggable = false;
                _this.user.mapObject.pixelMapperDragging = false;
                _this.user.mapObject = false;
                jQuery( ".app" ).removeClass( "dragging" );
            }
        } );

        jQuery( document ).on( "mousemove touchmove", function( e ) {
            _this.isPressed( e.originalEvent );
            if ( _this.map ) {

                _this.map.dragMap = false;

                if ( _this.tool == "bucket" ) {
                    _this.map.dragMap = false;

                } else if ( _this.user.leftClick && _this.tool == "pointer" && _this.user.mapObject ) {

                    if ( _this.user.mapObject.pixelMapperDraggable ) {
                        var zoomLevel = _this.map.zoomLevel();
                        var locked = _this.user.mapObject.locked === undefined ? true : _this.user.mapObject.locked;
                        var x = ( e.clientX - ( locked ? _this.map.mapContainer.x : 0 ) ) / zoomLevel;
                        var y = ( e.clientY - ( locked ? _this.map.mapContainer.y : 0 ) ) / zoomLevel;

                        if ( locked ) {
                            jQuery( _this.user.mapObject.displayObject.node ).attr( {
                                transform: "translate(" + x + "," + y + ") scale(" + _this.user.mapObject.displayObject.scale + ")"
                            } );
                        } else {
                            x = e.clientX;
                            y = e.clientY;
                            jQuery( _this.user.mapObject.displayObject.node ).attr( {
                                transform: "translate(" + x + "," + y + ")"
                            } );
                        }

                        _this.user.mapObject.pixelMapperDragging = true;
                        _this.map.dragMap = false;
                        jQuery( ".app" ).addClass( "dragging" );
                    }
                }
            }
        } );
    };


    /**
     * CHECKS IF GIVEN EVENT HAS BEEN THROWN WITH PRESSED CLICK / TOUCH
     */
    PMG.prototype.isPressed = function( event ) {
        var _this = this;

        // IE EXCEPTION
        if ( event.type == "mousemove" && event.which === 1 ) {
            // IGNORE

            // OTHERS
        } else if (
            event.type == "touchmove" ||
            event.buttons === 1 ||
            event.button === 1 ||
            event.which === 1
        ) {
            _this.user.leftClick = true;
        } else {
            _this.user.leftClick = false;
        }
        return _this.user.leftClick;
    };

    PMG.prototype.cover = function( cfg ) {
        var _this = this;
        var ring = jQuery( ".app-cover-ring" );
        var data = ring.data();
        var totalLength = 500;
        var startLength = totalLength * ( cfg.start || 0 ) / 100;
        var endLength = totalLength * ( cfg.end || 100 ) / 100;
        var duration = cfg.duration || _this.transition;

        if ( cfg.start === undefined ) {
            startLength = data.progress || 0;
        } else if ( cfg.start === 0 ) {
            jQuery( ".app" ).addClass( "cover" );
            if ( cfg.duration === undefined ) {
                duration = 2000;
            }
        }

        jQuery( ring ).stop();

        if ( cfg.text ) {
            jQuery( ".app-cover-text" ).text( cfg.text );
        }

        jQuery( ring ).prop( "number", startLength ).animateNumber( {
            number: endLength,
            easing: 'easeInQuad',
            numberStep: function( now, tween ) {
                jQuery( ring ).data( {
                    progress: now
                } ).attr( 'stroke-dashoffset', 500 - now );

                if ( now == totalLength ) {
                    jQuery( ".app" ).removeClass( "cover" );
                }
            }
        }, duration, cfg.complete );
    }

    /*
     ** LOAD MAP
     */
    PMG.prototype.loadMap = function( fileName, recall ) {
        var _this = this;
        var mapName = fileName.replace( ".js", "" );

        _this.user.loadMap = mapName;

        jQuery( ".app" ).addClass( "loading" );

        if ( _this.maps.indexOf( fileName ) == -1 ) {

            _this.maps.push( fileName );

            var node = document.createElement( "script" );
            node.setAttribute( "type", "text/javascript" );
            node.setAttribute( "src", "./static/lib/ammap/maps/js/" + fileName );
            node.addEventListener( "load", function() {
                _this.loadMap( fileName, true );
            } );
            document.head.appendChild( node );
            _this.cover( {
                text: "Loading...",
                start: 0,
                end: 90
            } );
            return;
        } else if ( !recall ) {
            _this.cover( {
                text: "Loading...",
                start: 0,
                end: 90
            } );
        }
        setTimeout( function() {
            // DESTROY PREVIOUS
            if ( _this.map ) {
                _this.map.destroy();
            }

            // CREATE MAP INSTANCE
            _this.map = pixelMapper.createMap( mapName );

            // ADD LISTENER
            _this.map.addListener( "init", function() {
                _this.cover( {
                    end: 100,
                    complete: function() {
                        jQuery( ".app" ).removeClass( "loading init" );
                    }
                } );
            } );
            _this.map.addListener( "drawn", function( e ) {
                clearTimeout( e.chart.pixelMapperTimeout );
                e.chart.pixelMapperTimeout = setTimeout( function() {
                    _this.cover( {
                        end: 100,
                        complete: function() {
                            jQuery( ".app" ).removeClass( "loading init generating" );
                        }
                    } );
                }, _this.transition );
            } );
            _this.map.addListener( "click", function( e ) {
                var map = e.chart;
                var color;
                var zoomLevel = map.zoomLevel();
                var position = {
                    latitude: map.coordinateToLatitude( ( e.y - map.mapContainer.y + ( AmCharts.isIE ? 0 : 13 ) ) / zoomLevel ),
                    longitude: map.coordinateToLongitude( ( e.x - map.mapContainer.x + ( AmCharts.isIE ? 0 : 13 ) ) / zoomLevel ),
                };

                pixelMapper.saveZoom();

                jQuery( ".map-toolbar .open" ).removeClass( "open" );

                // IMAGE
                if ( _this.tool == "image" ) {
                    var img = new AmCharts.MapImage();
                    img.chart = pixelMapper.map;
                    img.locked = true;
                    color = pixelMapper.handleColor( _this.defaults.color );

                    pixelMapper.selectedObject = jQuery.extend( img, {
                        color: color,
                        rollOverColor: color,
                        selectedColor: color,
                        latitude: position.latitude,
                        longitude: position.longitude,
                        svgPath: _this.defaults.svgPath,
                        selectable: true,
                        pixelMapperImage: true
                    } );

                    _this.map.dataProvider.zoomLevel = _this.map.zoomLevel();
                    _this.map.dataProvider.zoomLatitude = _this.map.zoomLatitude();
                    _this.map.dataProvider.zoomLongitude = _this.map.zoomLongitude();

                    _this.map.dataProvider.images.push( img );
                    img.validate();

                    _this.map.selectObject( img );
                    _this.createPopup( img );

                    // TEXT
                } else if ( _this.tool == "text" ) {
                    var txt = new AmCharts.MapImage();
                    txt.chart = pixelMapper.map;
                    txt.locked = true;
                    color = pixelMapper.handleColor( _this.defaults.color );

                    pixelMapper.selectedObject = jQuery.extend( txt, {
                        color: color,
                        labelShiftX: -15,
                        labelShiftY: -13,
                        width: 0.01,
                        height: 0.01,
                        label: "Text...",
                        labelPosition: "right",
                        labelColor: color,
                        labelRollOverColor: color,
                        labelFontSize: pixelMapper.map.fontSize,
                        latitude: position.latitude,
                        longitude: position.longitude,
                        selectable: true,
                        pixelMapperText: true
                    } );

                    _this.map.dataProvider.zoomLevel = _this.map.zoomLevel();
                    _this.map.dataProvider.zoomLatitude = _this.map.zoomLatitude();
                    _this.map.dataProvider.zoomLongitude = _this.map.zoomLongitude();

                    _this.map.dataProvider.images.push( txt );
                    txt.validate();

                    _this.map.selectObject( txt );
                    _this.createPopup( txt );

                } else if ( _this.tool != "line" ) {
                    _this.removePopups();
                    pixelMapper.map.selectObject( undefined, true );
                }
            } );

            _this.map.addListener( "clickMapObject", function( e ) {
                if ( _this.tool == "pointer" ) {
                    _this.createPopup( e.mapObject );
                }
            } );
            _this.map.addListener( "rollOverMapObject", function( e ) {
                if ( _this.tool == "bucket" && _this.user.leftClick ) {
                    pixelMapper.handleClickMapObject( e );
                }

                if ( !_this.user.mapObject && _this.tool == "pointer" && ( e.mapObject.pixelMapperImage || e.mapObject.pixelMapperText ) ) {
                    _this.user.mapObject = e.mapObject;
                    _this.user.mapObject.pixelMapperDraggable = true;
                }
            } );
            _this.map.addListener( "rollOutMapObject", function( e ) {
                if ( _this.user.mapObject && !_this.user.mapObject.pixelMapperDragging ) {
                    _this.user.mapObject.pixelMapperDragging = false;
                    _this.user.mapObject.pixelMapperDraggable = false;
                    _this.user.mapObject = false;
                }
            } );
        }, 500 );
    };

    /*
     ** WAIT FOR DOCUMENT; INITIATE PIXELMAPPER
     */
    jQuery( document ).ready( function() {
        window.PMG = new PMG();
    } );
} )();
